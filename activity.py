name = "Hillary Almonte"
age = 22
occupation = "Programming Instructor"
movie = "The Princess Diaries"
rating = 98.90

print("I am " + name + ", and I am " + str(age) + " years old, I work as a " + occupation + ", and my rating for " + movie + " is " + str(rating) + "%")

num1 = 9
num2 = 26
num3 = 19

print(f"The product of {num1} * {num2} is equal to {num1 * num2}")

print(f"Is {num1} less than {num3}: {num1 < num3}")

print(f"The sum of {num3} + {num2} is equal to {num3 + num2}")